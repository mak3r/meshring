

//ring width = approx. threadLength*sin(threadAngle)

threadRadius=.35;
ringRadius=10.5;
numThreads=32;
threadLength=16; //change this value to change the width of the ring. 
threadAngle=40;
rimWidth=1.8;
rimTranslate=.5*threadLength*sin(threadAngle);

translate([0,0,rimTranslate])
rim();
mesh();
translate([0,0,-(rimWidth+rimTranslate)])
rim();

module rim($fn=80) {
	difference() {
		cylinder(r=ringRadius+1.6,h=rimWidth);
		translate([0,0,-1])
		cylinder(r=ringRadius-1,h=rimWidth+2);
	}
}

module mesh($fn=16) {

	for ( i = [0 : numThreads] ) 
	{
		poleOne(i);
	}

	for ( i = [0 : numThreads] ) 
	{
		poleTwo(i);
	}

}

module poleOne(i=0) {
	rotate(i*360/numThreads, [0,0,1])
	rotate(threadAngle, [1,0,0])
	translate([ringRadius,0,0])
	translate([0,0,-threadLength/2])
	cylinder(r=threadRadius, h=threadLength);
}

module poleTwo(i=0) {
	rotate(i*360/numThreads, [0,0,1])
	rotate(-threadAngle, [1,0,0])
	translate([ringRadius,0,0])
	translate([0,0,-threadLength/2])
	cylinder(r=threadRadius, h=threadLength);
}